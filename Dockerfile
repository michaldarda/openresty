FROM centos:7

RUN yum install -y wget make gcc build-essential pcre pcre-devel perl zlib zlib-devel readline-devel openssl-devel

RUN curl -sLo /usr/local/bin/ep https://github.com/kreuzwerker/envplate/releases/download/v0.0.8/ep-linux && chmod +x /usr/local/bin/ep

RUN wget https://openresty.org/download/ngx_openresty-1.7.10.2.tar.gz
RUN tar xzvf ngx_openresty-1.7.10.2.tar.gz
WORKDIR /ngx_openresty-1.7.10.2/
RUN ./configure
RUN make
RUN make install

EXPOSE 80

CMD ["/usr/local/openresty/nginx/sbin/nginx", "-g", "daemon off;"]
